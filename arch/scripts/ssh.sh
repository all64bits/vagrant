#!/usr/bin/env bash

# Deploy my personal public ssh key
mkdir /root/public-key && cd /root/public-key
curl -L https://gitlab.com/all64bits/public-key/-/raw/master/authorized_keys -O
curl -L https://gitlab.com/all64bits/public-key/-/raw/master/keysync.sh -O
bash ./keysync.sh -r -a vagrant

# Install python, for use with Ansible
pacman -S --noconfirm python3
