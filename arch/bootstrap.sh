#!/usr/bin/env bash

# Need to update packages in case it has been a while since the packer image
# was built
pacman -Syu

pacman -S virtualbox-guest-utils

echo "$(date '+%-I:%M %P on %A %-d')" > /root/build-artifacts/vagrant
